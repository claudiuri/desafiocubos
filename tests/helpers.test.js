const { getDayOfDate, foramattedDate, createDateFormatted, validateDates } = require('../helpers/helpers');

describe('getDayOfDate', () =>{

    it('should return 0 if the date is a sunday', () =>{
        expect(getDayOfDate("18-08-2019")).toBe(0);
    });

    it('should return 1 if the date is a monday', () =>{
        expect(getDayOfDate("19-08-2019")).toBe(1);
    });

    it('should return 2 if the date is a tuesday', () =>{
        expect(getDayOfDate("20-08-2019")).toBe(2);
    });

    it('should return 3 if the date is a wednesday', () =>{
        expect(getDayOfDate("21-08-2019")).toBe(3);
    });

    it('should return 4 if the date is a thursday', () =>{
        expect(getDayOfDate("22-08-2019")).toBe(4);
    });

    it('should return 5 if the date is a friday', () =>{
        expect(getDayOfDate("23-08-2019")).toBe(5);
    });

    it('should return 6 if the date is a saturday', () =>{
        expect(getDayOfDate("24-08-2019")).toBe(6);
    });
});

describe('foramattedDate', () =>{

    let dateString = "18/08/2019";
    let newDate = '';
    
    it('should return undefined if date has invalid format', () =>{

        expect(foramattedDate(dateString)).toBeUndefined();
    });

    it('should return undefined if date is less than 10 characters', () =>{

        newDate = dateString.substring(0, 8);
        expect(foramattedDate(newDate)).toBeUndefined();
    });

    it('should return date formated if date is valid', () =>{
        newDate = dateString.split("/").join("-")
        expect(foramattedDate(newDate)).toBe(dateString.split("/").reverse().join("-"));
    });
    
});
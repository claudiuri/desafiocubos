const express = require('express');
const rulesController = require('./controllers/rules')

const routes = express.Router();

routes.post('/rules', rulesController.create);
routes.get('/rules', rulesController.find);
routes.delete('/rules/:id', rulesController.remove);

module.exports = routes;
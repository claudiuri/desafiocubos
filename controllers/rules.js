const { create, find, removeById } = require('../models/rules');

module.exports = {

    async create (req, res){
        try {
            const rule = await create(req.body);
    
            res.status(201).json(rule);
    
        } catch (err) {
            res.status(400).json({ error: err.message })
        }
    },

    async find(req, res) {
        try {
            const rules = await find(req.query);
    
            res.set('Content-Type', 'application/json; charset=UTF-8').status(200).send(rules);
    
        } catch (err) {
            res.status(400).json({ error: err.message })
        }
    },

    async remove(req, res){
        try {
            await removeById(req.params.id);
    
            res.status(200).json({message: "Regra removida com sucesso"});
    
        } catch (err) {
            res.status(err.statusCode).json({ error: err.message })
        }
    }

}
const fs = require('fs');
const moment = require('moment');

function getAllRules (){
    return new Promise((resolve, reject) =>{
        fs.readFile('rules.json', {encoding: "utf8"}, (err, data) =>{

            if (err) return reject(err);

            resolve(JSON.parse(data));
        });
    });
}

const save = (content) => {
   fs.writeFileSync('rules.json', JSON.stringify(content), 'utf8', (err) => {
       if (err) {
           console.log(err)
       }
   })
}

const getDayOfDate = (date) => { return createDateFormatted(date).getUTCDay(); }

const foramattedDate = (date) => { 
    if(date.includes("-") && date.length === 10){

        return date.split("-").reverse().join("-"); 
    }

    return undefined;
}

const createDateFormatted = (date) => { return new Date(foramattedDate(date)) };

const validateDates = (dateStart, dateEnd) => {
   
    if(dateStart && !dateEnd || !dateStart && dateEnd){ 
        throw Error("Informe a data de início e fim");
    }

    if(moment(dateStart, 'DD-MM-YYYY').format('DD-MM-YYYY') !== dateStart || 
        moment(dateEnd, 'DD-MM-YYYY').format('DD-MM-YYYY') !== dateEnd){
        throw Error("Informe a data no formato DD-MM-YYYY");
    }
    
    if(createDateFormatted(dateStart) >  createDateFormatted(dateEnd)) {
        throw Error("Data final deve ser maior que a de início");
    }

    return;
}

module.exports =  {
   save,
   getAllRules,
   getDayOfDate,
   validateDates,
   foramattedDate,
   createDateFormatted,
};
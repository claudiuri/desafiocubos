const { save,  getAllRules,  validateDates } = require('../helpers/helpers');

const uuid  = require('uuid');
const moment  = require('moment');

function create (newRule){

    return new Promise(async (resolve, reject) =>{

        let rules = await getAllRules();
        let erroMessage = "Já possui um regra cadastrada nesse horário";

        //#region  Valida regra
        for(let rule of rules){
                
            for(let interval of rule.intervals){
                for(let newRuleInterval of newRule.intervals){
                    if(!(interval.start > newRuleInterval.end ||interval.end < newRuleInterval.start)){
                        
                        if (newRule.type === 'daily' || rule.type === 'daily') {
                            reject(Error(erroMessage))
                        }
                        
                        if(newRule.type === 'only-day' && rule.type === 'only-day'){
                            if(newRule.days[0] === rule.days[0]){
                                reject(Error(erroMessage))
                            }
                        }
                        else if(newRule.type === 'weekly' && rule.type === 'only-day' || newRule.type === 'only-day' && rule.type === 'weekly'){
                            if(newRule.days.indexOf(moment(rule.days[0], 'DD-MM-YYYY').weekday()) >= 0 || rule.days.indexOf(moment(newRule.days[0], 'DD-MM-YYYY').weekday()) >= 0){
                                reject(Error(erroMessage))
                            }
                        }
                        else if(newRule.type === 'weekly' && rule.type === 'weekly'){
                            
                            for(let day of newRule.days){
                                
                                if(rule.days.indexOf(day) >=0){
                                    reject(Error(erroMessage))
                                }
                            }
                            
                        }
                        
                    }
                }
            }
        }
        //#endregion

        const id = uuid();
        newRule = Object.assign({ id }, newRule);
        rules.push(newRule);
        save(rules)
        resolve(newRule)
    });
}

function find (object){
    let { dateStart, dateEnd } = object;

    if (dateStart || dateEnd) {

        validateDates(dateStart, dateEnd)

        return new Promise(async (resolve, reject) => {
        
            let rules = await getAllRules();

            let filter = [];

            dateStart = moment(dateStart, 'DD-MM-YYYY');
            dateEnd = moment(dateEnd, 'DD-MM-YYYY');

            const diffDays = dateEnd.diff(dateStart, 'days');
            
            for (let i = 0; i <= diffDays; i++) {
                rules.map(rule => {

                    if (rule.type === 'only-day') {
                        if (moment(rule.days[0], 'DD-MM-YYYY').format('DD-MM-YYYY') === dateStart.format('DD-MM-YYYY')) {
                            filter = addDayInFitler(dateStart.format('DD-MM-YYYY'), rule.intervals, filter)
                        }
                    }
                    else if (rule.type === 'daily') {
                        filter = addDayInFitler(dateStart.format('DD-MM-YYYY'), rule.intervals, filter)
                    }
                    else if (rule.type === 'weekly') {
                        if (rule.days.indexOf(moment(dateStart, 'DD-MM-YYYY').weekday()) >= 0) {
                            filter = addDayInFitler(dateStart.format('DD-MM-YYYY'), rule.intervals , filter)
                        }
                    }
                });

                dateStart.add(1, 'days')
            }

            resolve(filter)
        });
    }

    return getAllRules();
}

function removeById (id){
    return new Promise(async (resolve, reject) => {
        let rules = await getAllRules();

        if(!rules.find(rule => rule.id === id)){
            reject({statusCode: 404, message: "Regra de atendimento não encontrada" });
        } 

        rules = rules.filter(rule => rule.id !== id);
        save(rules)
        resolve()
    });
}

function addDayInFitler (day, intervals, filter) {
    
    let newRule = { day, intervals };
    let wasAdded = false
   
    if (filter.length > 0) {

        filter.forEach((rule, index) =>{
            if (rule.day === newRule.day) {

                wasAdded = true
                filter[index].intervals = rule.intervals.concat(newRule.intervals)
            }
        });
    } 
    else {
        wasAdded = true
        filter.push(newRule)
    }

    if(!wasAdded){
        filter.push(newRule)
    }

    return filter
}

module.exports = {
    create,
    find,
    removeById
}
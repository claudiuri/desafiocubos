require('dotenv').config();
const express = require('express');
const fs = require('fs');
const app = express();
const { getAllRules } = require('./helpers/helpers')

app.use(express.json());

app.get('/', (req,res) =>{
    res.json({
        name: "api-clinicas",
        version: "1.0.0"
    })
});

app.use('/api', require('./routes'));
app.listen(process.env.PORT, async() =>{
    console.log(`Server on http://localhost:${ process.env.PORT }/`);

    
    fs.exists('rules.json', (exists) =>{
        if (!exists) {
            var obj = [];
            fs.writeFile('rules.json', JSON.stringify(obj), (err) => {
                if(err){
                    throw err;
                }
                console.log("Arquivo json criado com sucesso :)");
            });
        }
    });
})
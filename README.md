# api-clinicas
API REST para facilitar o gerenciamento de horários de uma clínica!

## Instalação
* [Baixe](https://gitlab.com/claudiuri/desafiocubos/-/archive/master/desafiocubos-master.zip) o projeto ou dê um `gitclone`
* Acesse o a pasta do projeto pelo terminal
* Postman https://www.getpostman.com/collections/d3436d7382040f2a2f17
#### Execute o comando 'npm install' para instalar as dependências do projeto
```
 npm install
```
## Executar testes
```
 npm run test
```
## Variáveis de ambiente
```
 PORT=3333
```

## Execução da API 
Depois de [instalar](#instalação) e criar as variáveis de ambiente basta executar o comando `npm start` no terminal e pronto! A api esta em `http://localhost:3333/` ! :rocket:

## Como funciona

| rotas                    | descrição                 |
|:-----------------------------|:----------------------------|
| api/rules    `POST`                  | criação de regra de atendimento |
| api/rules    `GET`                  | listagem de regras de atendimos |
| api/rules/:id    `DEL`                  | remoção de regra de atendimento |
| api/rules?dateStart=11-08-2019&dateEnd=12-08-2019    `GET`          | listagem de horários |

## Cadastro da regra 
`http://localhost:3333/api/rules POST 201`
| Campo | Tipo | Obs |
| ----------- | ----------- | ----------- |
| type | STRING |  |
| days | ARRAY |  |
| intervals | ARRAY | |
| intervals.start | STRING | hh/mm |
| intervals.end | STRING | hh/mm |

### Tipos de regra de atendimento 'type'
| Tipo de regra | Exemplo 
| ----------- | ----------- 
| Um dia específico | `only-day` |
| Diariamente | `daily` |
| Semanalmente | `weekly` |

### Dias de atendimento 'days'
| type | Exemplo | 
| ----------- | -----------  
|"only-day" | ['DD-MM-YYYY'] |
| "daily" | ['All'] | 
| "weekly" | [0, 6] Dom..Sáb | 

Cadastro de um regra com 'type' `only-day`
```

  {
    "type": "only-day",
    "days":["25/08/2019"],
    "intervals": [{"start": "10:23", "end": "11:00" }]
  }

```
Cadastro de um regra com 'type' `daily`
```

  {
    "type": "daily",
    "days":["All"],
    "intervals": [{"start": "10:23", "end": "11:00" }]
  }

```

Cadastro de um regra com 'type' `weekly`
```

  {
    "type": "weekly",
    "days":[1, 3],
    "intervals": [{"start": "10:23", "end": "11:00" }]
  }

```
## Listagem das regras 
`http://localhost:3333/api/rules GET 200`

```
[
    {
        "id": "f3b873f6-e3e1-4fdb-8dc2-d3c47857824d",
        "type": "only-day",
        "days": [
            "06-07-2019"
        ],
        "intervals": [
            {
                "start": "12:00",
                "end": "13:00"
            }
        ]
    },
    {
        "id": "bdd41451-af46-4c0d-b15b-fb09439dcbf9",
        "type": "only-day",
        "days": [
            "07-07-2019"
        ],
        "intervals": [
            {
                "start": "12:00",
                "end": "13:00"
            }
        ]
    },
    {
        "id": "ff2ef0e3-5346-4190-9bfe-3121c5953d6a",
        "type": "only-day",
        "days": [
            "08-07-2019"
        ],
        "intervals": [
            {
                "start": "12:00",
                "end": "13:00"
            }
        ]
    },


```
## Listagem de horários 
`http://localhost:3333/api/rules?dateStart=18-08-2019&startEnd=21-08-2019 GET 200`

```

[
    {
        "day": "18-08-2019",
        "intervals": [
            {
                "start": "11:10",
                "end": "12:00"
            },
            {
                "start": "09:10",
                "end": "10:00"
            }
        ]
    },
    {
        "day": "19-08-2019",
        "intervals": [
            {
                "start": "11:10",
                "end": "12:00"
            },
            {
                "start": "11:10",
                "end": "12:00"
            },
            {
                "start": "11:10",
                "end": "12:00"
            },
            {
                "start": "09:10",
                "end": "10:00"
            }
        ]
    },
    .
    .
    .
    .
    .
]

```

## Remoçao de regras 
`http://localhost:3333/api/rules/:id DELETE 200`

```

{
  "message": "Regra removida com sucesso"
}

```
